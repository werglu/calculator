using ConsoleApp1;
using System;
using Xunit;

namespace XUnitTestProject1
{
    public class UnitTest1
    {
        [Fact]
        public void EmptyStringShouldReturnsZero()
        {
            var calc = new StringCalculator();
            var result = calc.Count("");
            Assert.Equal(0, result);
        }

        [Fact]
        public void ASingleNumberShouldReturnsValue()
        {
            var calc = new StringCalculator();
            Assert.Equal(2, calc.Count("2"));
            Assert.Equal(3, calc.Count("3"));
            Assert.Equal(4, calc.Count("4"));
            Assert.Equal(5, calc.Count("5"));
            Assert.Equal(6, calc.Count("6"));
        }

        [Fact]
        public void TwoNumbersCommaDelimitedReturnsSum()
        {
            var calc = new StringCalculator();
            Assert.Equal(3, calc.Count("1,2"));
            Assert.Equal(2, calc.Count("1,1"));
            Assert.Equal(10, calc.Count("8,2"));
        }

        [Fact]
        public void TwoNumbersNewLineDelimitedReturnsSum()
        {
            var calc = new StringCalculator();
            Assert.Equal(3, calc.Count("1/n2"));
            Assert.Equal(2, calc.Count("1/n1"));
            Assert.Equal(90, calc.Count("88/n2"));
        }

        [Fact]
        public void ManyNumbersNewLineDelimitedReturnsSum()
        {
            var calc = new StringCalculator();
            Assert.Equal(10, calc.Count("1/n2,3,4"));
            Assert.Equal(16, calc.Count("1/n1/n14"));
            Assert.Equal(102, calc.Count("88/n2,6,6"));
        }

        [Fact]
        public void NegativeNumberShouldThrowsException()
        {
            var calc = new StringCalculator();
            Assert.Throws<Exception>(() => calc.Count("-1"));
            Assert.Throws<Exception>(() => calc.Count("1/n1/n-14"));
            Assert.Throws<Exception>(() => calc.Count("88/n2,-6,6"));
        }

        [Fact]
        public void NumbersGreatherThan1000ShouldBeIgnored()
        {
            var calc = new StringCalculator();
            Assert.Equal(0, calc.Count("1001"));
            Assert.Equal(1015, calc.Count("1/n1000/n14"));
            Assert.Equal(96, calc.Count("88/n2,6,6000"));
        }

        [Fact]
        public void ASingleCharDelimitedCanBeDetected()
        {
            var calc = new StringCalculator();
            Assert.Equal(3, calc.Count("//#1#2"));
            Assert.Equal(6, calc.Count("//*1/n1*4"));
            Assert.Equal(97, calc.Count("//d88/n2,5d2"));
        }

        [Fact]
        public void AMultiCharDelimitedCanBeDetected()
        {
            var calc = new StringCalculator();
            Assert.Equal(3, calc.Count("//[##]1##2"));
            Assert.Equal(6, calc.Count("//[***]1/n1***4"));
            Assert.Equal(97, calc.Count("//[##d]88/n2,5##d2"));
        }
    }
}
