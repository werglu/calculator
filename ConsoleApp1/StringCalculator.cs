﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class StringCalculator
    {
       public int Count(string s)
       {    
            if (s.Length == 0) return 0;
            string special = ",";
            if(s[0] == '/' && s[1] == '/')
            {
                if (s[2] == '[')
                {
                    int ind = s.IndexOf(']');
                    special = s.Substring(3, ind - 3);
                    s = s.Substring(ind + 1, s.Length - (ind + 1));

                }
                else
                {
                    special = s[2].ToString();
                    s = s.Substring(3, s.Length - 3);
                }
            }
            int result = 0;
            var numbers = s.Split(new[] { ",", "/n", "/r", special }, StringSplitOptions.None);

            for(int i=0; i< numbers.Length; i++)
            {
                int num = int.Parse(numbers[i]);
                if (num < 0) throw new Exception("negaive number");
                if(num <= 1000)
                    result += num;
            }

            return result;
       }
    }
}
